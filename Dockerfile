# Base image to build the app image
# calling this build stage as "builder"
FROM node:16-alpine as builder 
WORKDIR '/app'
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

# After finishing with the first Base image
# to build the app image we use SECOND base image
# to omit all previous build except what we need
# which is in "/app/build" and copping it to the relevant
# production directory as mentioned hub.docker.com/_/nginx
# FROM nginx
# COPY static-html-directory /usr/share/nginx/html
FROM nginx
COPY --from=builder /app/build /usr/share/nginx/html